var Server = require('./config/server');

Server.start((server) => {
	console.log('Server running at ' + server.info.uri)
});