FROM node:latest

RUN mkdir -p /src

COPY . /src
WORKDIR /src
ADD package.json /src/package.json
RUN npm install

CMD node ./index.js