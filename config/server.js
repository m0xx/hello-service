'use strict';

const Hapi = require('hapi');
const Store = require('./store');
const routes = require('./routes');
const plugins = require('./plugins');

const server = new Hapi.Server();


server.connection({
    host: Store.get('/server/host'),
    port: Store.get('/server/port')
});


server.route(routes)

let Server = {};
Server.start = function(cb) {

	server.register(plugins, (err) => {
	    if (err) {
	        throw err;
	    }

	    server.start().then(function() {
	    	if(cb) {
				cb(server);
	    	}
	    });
	});
}

module.exports = Server;