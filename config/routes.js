'use strict';

const HomeController = require('./../lib/controller/home');

module.exports = [
	{path: '/', method: 'GET', config: HomeController.home }
]