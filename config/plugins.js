'use strict';

module.exports = [
    {
        register: require('hapi-swaggered'),
        options: {
            requiredTags: [],
            tags: {
                'api': 'hello-service'
            },
            info: {
                title: 'hello-service',
                description: 'hello-service',
                version: '1.0'
            },
        }
    }
]